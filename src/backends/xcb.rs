use crate::errors::*;
use crate::wm::enums::Event;
use crate::wm::traits::Backend;
use log::{info, debug, trace, warn};
use xcb;

pub struct XcbBackend {
    connection: xcb::Connection,
    root_window: xcb::Window
}

impl XcbBackend {
    fn create_window(&self, event: &xcb::GenericEvent) -> Event<xcb::Window> {
        let map_request: &xcb::MapRequestEvent = unsafe { xcb::cast_event(event) };
        debug!("XCB map request for new window {:?}", map_request.window());
        xcb::map_window(&self.connection, map_request.window());
        self.connection.flush();
        Event::WindowCreated(map_request.window())
    }

    fn destroy_window(&self, event: &xcb::GenericEvent) -> Event<xcb::Window> {
        let destroy_notify: &xcb::DestroyNotifyEvent = unsafe { xcb::cast_event(event) };
        debug!("XCB destroy notify for window {:?}", destroy_notify.window());
        xcb::map_window(&self.connection, destroy_notify.window());
        self.connection.flush();
        Event::WindowClosed(destroy_notify.window())
    }

    fn acquire_root_window(connection: &xcb::Connection, screen_number: i32) -> xcb::Window {
        let setup = connection.get_setup();
        let screen = setup.roots().nth(screen_number as usize).unwrap();
        info!("Screen dimensions: {}x{}",
              screen.width_in_pixels(),
              screen.height_in_pixels());
        screen.root()
    }

    fn set_event_mask(connection: &xcb::Connection, root_window: xcb::Window) {
        debug!("Setting root window properties");
        let values =
            [(xcb::CW_EVENT_MASK,
              xcb::EVENT_MASK_SUBSTRUCTURE_NOTIFY | xcb::EVENT_MASK_SUBSTRUCTURE_REDIRECT |
              xcb::EVENT_MASK_STRUCTURE_NOTIFY | xcb::EVENT_MASK_ENTER_WINDOW |
              xcb::EVENT_MASK_LEAVE_WINDOW |
              xcb::EVENT_MASK_PROPERTY_CHANGE | xcb::EVENT_MASK_BUTTON_PRESS |
              xcb::EVENT_MASK_BUTTON_RELEASE | xcb::EVENT_MASK_FOCUS_CHANGE)];

        {
            let cookie = xcb::change_window_attributes(connection, root_window, &values);
            let _ = cookie.request_check();
        }
    }
}

impl Backend for XcbBackend {
    type Window = xcb::Window;

    fn new() -> Result<XcbBackend> {
        info!("Instantiating XCB backend");
        info!("Connecting to default X Display");
        let (xconn, screen_num) = xcb::Connection::connect(None)
            .map_err(|_| "Unable to connect to display")?;

        let root_win = XcbBackend::acquire_root_window(&xconn, screen_num);
        debug!("Acquired root window {:?}", root_win);
        XcbBackend::set_event_mask(&xconn, root_win);
        xconn.flush();

        Ok(XcbBackend {
            connection: xconn,
            root_window: root_win,
        })
    }

    fn event(&self) -> Event<Self::Window> {
        trace!("Waiting for next X event");
        let event = self.connection.wait_for_event();

        match event {
            Some(event) => {
                let response_type = event.response_type();
                match response_type {
                    xcb::MAP_REQUEST => self.create_window(&event),
                    xcb::DESTROY_NOTIFY => self.destroy_window(&event),
                    _ => {
                        warn!("Unknown request {:?}", response_type);
                        Event::Unknown
                    }
                }
            }
            _ => Event::Unknown,
        }
    }

    // Information queries
    fn is_dock(&self, window: Self::Window) -> bool {
        false
    }

    fn is_window(&self, window: Self::Window) -> bool {
        false
    }

    // Window manipulations
    fn resize_window(&self, window: Self::Window, width: u32, height: u32) {
        let values = [(xcb::CONFIG_WINDOW_WIDTH as u16, width),
                        (xcb::CONFIG_WINDOW_HEIGHT as u16, height)];
        xcb::configure_window(&self.connection, window, &values);
        self.connection.flush();
    }

    fn move_window(&self, window: Self::Window, x: u32, y: u32) {
        let values = [(xcb::CONFIG_WINDOW_X as u16, x),
                        (xcb::CONFIG_WINDOW_Y as u16, y)];
        xcb::configure_window(&self.connection, window, &values);
        self.connection.flush();
    }

    fn show_window(&self, window: Self::Window) {
        xcb::map_window(&self.connection, window);
    }

    fn hide_window(&self, window: Self::Window) {
        xcb::unmap_window(&self.connection, window);
    }

    fn focus_window(&self, window: Self::Window) {
       xcb::set_input_focus(&self.connection, 0, window, xcb::CURRENT_TIME); 
    }
}
