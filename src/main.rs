// External crates (with macro_use)
#[macro_use]
extern crate error_chain;

// Project modules
mod backends;
mod errors {
    error_chain! {
        foreign_links {
            Io(::std::io::Error);
            Log(::log::SetLoggerError);
        }
    }
}
mod wm;

// Module use statements
use crate::backends::XcbBackend;
use crate::errors::*;
use crate::wm::enums::Event;
use crate::wm::traits::Backend;
use crate::wm::workspace::Workspace;
use log::{info, error};
use xdg::BaseDirectories;

fn main() {
    if let Err(ref e) = run() {
        error!("SegWM stopped: {}", e);
        for e in e.iter().skip(1) {
            error!("caused by: {}", e);
        }

        if let Some(backtrace) = e.backtrace() {
            error!("backtrace: {:?}", backtrace);
        }

        std::process::exit(1);
    }
}

fn run() -> Result<()> {
    initialize_logger()
        .chain_err(|| "Unable to initialize logger")?;

    let xcb = XcbBackend::new()?;
    let mut ws = Workspace::new(0, "Main", None);
    let mut window_count = 0;

    loop {
        match xcb.event() {
            Event::WindowCreated(window) => {
                ws = ws.add(window);
                window_count = window_count + 1;
                if (window_count%2) == 1 {
                    xcb.move_window(window, 0, 0);
                } else {
                    xcb.move_window(window, 960, 0);
                }
                xcb.resize_window(window, 960, 1080);
            }
            Event::WindowClosed(window) => {
                ws = ws.remove(window);
                window_count = window_count - 1;
            }
            _ => (),
        }
    }
}

fn initialize_logger() -> Result<()> {
    let xdg_base = BaseDirectories::with_prefix("segwm")
        .chain_err(|| "Unable to get XDG base directory")?;
    let log_path = xdg_base.place_cache_file("segwm.log")
        .chain_err(|| "Unable to get path for log file")?;

    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!("{}[{}][{}] {}",
                                    chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
                                    record.target(),
                                    record.level(),
                                    message))
            })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .chain(fern::log_file(log_path)?)
        .apply()?;

        info!("Initialized logger");
        info!("SegWM version {}", env!("CARGO_PKG_VERSION"));

    Ok(())
}
