use crate::wm::stack::Stack;
use log::{debug, trace};
use std::fmt::Debug;

pub struct Workspace<Window> {
    id: u32,
    tag: String,
    stack: Option<Stack<Window>>,
}

impl<Window: Clone> Clone for Workspace<Window> {
    fn clone(&self) -> Workspace<Window> {
        Workspace {
            id: self.id,
            tag: self.tag.clone(),
            stack: self.stack.clone(),
        }
    }
}

impl<Window: Copy + Clone + PartialEq + Eq + Debug> Workspace<Window> {
    pub fn new<S: Into<String>>(id: u32,
                                tag: S,
                                stack: Option<Stack<Window>>)
                                -> Workspace<Window> {
        let tag = tag.into();
        trace!("[WS {}:{}]: Creating new workspace", id, tag);

        Workspace {
            id: id,
            tag: tag,
            stack: stack,
        }
    }

    pub fn add(&self, window: Window) -> Workspace<Window> {
        debug!("[WS {}:{}]: Adding window {:?} to workspace",
               self.id, self.tag, window);
        
        Workspace::new(self.id,
                       self.tag.clone(),
                       Some(self.stack
                            .clone()
                            .map_or(Stack::from(window), |s| s.add(window))))
    }

    pub fn remove(&self, window: Window) -> Workspace<Window> {
        debug!("[WS {}:{}]: Removing window {:?} from workspace",
               self.id, self.tag, window);
        
        Workspace::new(self.id,
                       self.tag.clone(),
                       self.stack
                            .clone()
                            .map_or(None, |s| s.filter(|&w| w != window)))
    }

    pub fn len(&self) -> usize {
        self.stack.clone().map_or(0, |x| x.len())
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn contains(&self, window: Window) -> bool {
        trace!("[WS {}:{}]: checking if ws contains window {:?}",
               self.id,
               self.tag,
               window);
        self.stack.clone().map_or(false, |x| x.contains(window))
    }

    pub fn windows(&self) -> Vec<Window> {
        self.stack.clone().map_or(Vec::new(), |s| s.integrate())
    }

    pub fn peek(&self) -> Option<Window> {
        self.stack.clone().map(|s| s.focus)
    }

    pub fn map<F>(&self, f: F) -> Workspace<Window>
        where F: Fn(Stack<Window>) -> Stack<Window>
    {
        trace!("[WS {}:{}]: mapping over workspace",
               self.id,
               self.tag);

        Workspace::new(self.id,
                       self.tag.clone(),
                       self.stack.clone().map(f))
    }

    pub fn map_option<F>(&self, f: F) -> Workspace<Window>
        where F: Fn(Stack<Window>) -> Option<Stack<Window>>
    {
        trace!("[WS {}:{}]: mapping optional over workspace",
               self.id,
               self.tag);
        
        Workspace::new(self.id,
                       self.tag.clone(),
                       self.stack.clone().map_or(None, f))
    }

    pub fn map_or<F>(&self, default: Stack<Window>, f: F) -> Workspace<Window>
        where F: Fn(Stack<Window>) -> Stack<Window>
    {
        trace!("[WS {}:{}]: mapping default over workspace",
               self.id,
               self.tag);
        
        Workspace::new(self.id,
                       self.tag.clone(),
                       Some(self.stack.clone().map_or(default, f)))
    }
}
