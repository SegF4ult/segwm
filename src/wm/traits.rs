use crate::errors::*;
use crate::wm::enums::Event;

pub trait Backend {
    type Window;
    
    fn new() -> Result<Self> where Self: ::std::marker::Sized;
    
    fn event(&self) -> Event<Self::Window>;
    
    // Information queries
    fn is_dock(&self, window: Self::Window) -> bool;
    
    fn is_window(&self, window: Self::Window) -> bool;
    
    // Window manipulations
    fn resize_window(&self, window: Self::Window, width:u32, height: u32);
    
    fn move_window(&self, window: Self::Window, x: u32, y: u32);
    
    fn show_window(&self, window: Self::Window);
    
    fn hide_window(&self, window: Self::Window);
    
    fn focus_window(&self, window: Self::Window);
}
