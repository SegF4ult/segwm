#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Event<Window> {
    BackendChanged,
    WindowCreated(Window),
    WindowClosed(Window),
    WindowHidden(Window),
    WindowRevealed(Window),
    //WindowChangeRequest(Window, Rectangle),
    MouseEnter(Option<Window>),
    MouseExit(Option<Window>),
    ButtonPressed(Window, Option<Window>),
    ButtonReleased,
    KeyPressed(Window),
    Unknown,
}

